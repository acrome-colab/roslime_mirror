#### By Can Altineller <altineller@gmail.com>

#### How to use this package:

After installing ROS on Ubuntu, following: http://wiki.ros.org/melodic/Installation/Ubuntu open a new terminal and:

```
mkdir -p ~/catkin_ws/src

cd ~/catkin_ws/src

git clone https://github.com/altineller/roslime_description.git

cd ~/catkin_ws

catkin_make

source ~/catkin_ws/devel/setup.bash
```

To use this model in your simulation, include `model.launch` in your own launch file

To see a demo:

```
roslaunch roslime_description demo.launch
```

Which will launch model, the required static transforms, rviz, and joint_state_publisher gui.
You can turn wheels by changing the sliders on the joint_state_publisher gui.

![alt text](https://raw.githubusercontent.com/ROSLime/roslime_documentation/master/images/description/ROSLime_description.png)

